# -*- coding: utf-8 -*-
import unittest
import numpy as np

import calculate

i = lambda *ds: np.zeros(ds, dtype=int, order="F")
r32 = lambda *ds: np.zeros(ds, dtype=np.float32, order="F")
r64 = lambda *ds: np.zeros(ds, dtype=np.float64, order="F")
c32 = lambda *ds: np.zeros(ds, dtype=np.complex64, order="F")
c64 = lambda *ds: np.zeros(ds, dtype=np.complex128, order="F")
rF = lambda *ds: np.zeros(ds, order="F")
rC = lambda *ds: np.zeros(ds, order="C")
cF = lambda *ds: np.zeros(ds, dtype=np.complex128, order="F")
cC = lambda *ds: np.zeros(ds, dtype=np.complex128, order="C")
r = rF
c = cF


class CalculateModuleInterfaceTest(unittest.TestCase):

    @staticmethod
    def get_calculate_rho_k():
        return getattr(calculate, "calculate_rho_k")

    @staticmethod
    def get_calculate_rho_j_k():
        return getattr(calculate, "calculate_rho_j_k")

    def assertDoesNotRaise(self, fun, *args):
        try:
            fun(*args)
            success = True
        except:
            success = False

        self.assertTrue(success, "%s(%s) raised exception" % (fun.__name__, ','.join(map(str, args))))

    def test_rho_k_throw_if_not_same_float_dtype(self):
        rho_k = self.get_calculate_rho_k()

        self.assertRaisesRegex(RuntimeError,
                               r"internal_calculate has unsupported data value type",
                               rho_k, i(5), c32(3, 5), r32(3, 2))
        self.assertRaisesRegex(RuntimeError,
                               r"k has unsupported data value type",
                               rho_k, c32(5), i(3, 5), r32(3, 2))
        self.assertRaisesRegex(RuntimeError,
                               r"x has unsupported data value type",
                               rho_k, c32(5), c32(3, 5), i(3, 2))

        self.assertRaisesRegex(RuntimeError,
                               r"Data value type mismatch internal_calculate:double, k:float, x:double",
                               rho_k, c64(5), c32(3, 5), r64(3, 2))
        self.assertRaisesRegex(RuntimeError,
                               r"Data value type mismatch internal_calculate:float, k:float, x:double",
                               rho_k, c32(5), c32(3, 2), r64(3, 5))

    def test_rho_k_throw_if_not_correct_number_type(self):
        rho_k = self.get_calculate_rho_k()

        self.assertRaisesRegex(RuntimeError,
                               r"internal_calculate does not hold complex-numbers",
                               rho_k, r32(5), r32(3, 5), r32(3, 2))
        self.assertRaisesRegex(RuntimeError,
                               r"k does not hold real-numbers",
                               rho_k, c32(5), c32(3, 5), r32(3, 2))
        self.assertRaisesRegex(RuntimeError,
                               r"x does not hold real-numbers",
                               rho_k, c32(5), r32(3, 5), c32(3, 2))

    def test_rho_j_k_throw_if_not_correct_number_type(self):
        rho_j_k = self.get_calculate_rho_j_k()

        self.assertRaisesRegex(RuntimeError,
                               r"internal_calculate does not hold complex-numbers",
                               rho_j_k, r32(5), c32(3, 5), r32(3, 5), r32(3, 2), r32(3, 2))
        self.assertRaisesRegex(RuntimeError,
                               r"j_k does not hold complex-numbers",
                               rho_j_k, c32(5), r32(3, 5), r32(3, 5), r32(3, 2), r32(3, 2))
        self.assertRaisesRegex(RuntimeError,
                               r"k does not hold real-numbers",
                               rho_j_k, c32(5), c32(3, 5), c32(3, 5), r32(3, 2), r32(3, 2))
        self.assertRaisesRegex(RuntimeError,
                               r"x does not hold real-numbers",
                               rho_j_k, c32(5), c32(3, 5), r32(3, 5), c32(3, 2), r32(3, 2))
        self.assertRaisesRegex(RuntimeError,
                               r"v does not hold real-numbers",
                               rho_j_k, c32(5), c32(3, 5), r32(3, 5), r32(3, 2), c32(3, 2))

    def test_rho_j_k_throw_if_not_same_float_dtype(self):
        rho_j_k = self.get_calculate_rho_j_k()

        self.assertRaisesRegex(RuntimeError,
                               r"Data value type mismatch internal_calculate:double, j_k:double, k:float, x:double, v:double",
                               rho_j_k, c64(5), c64(3, 5), r32(3, 5), r64(3, 2), r64(3, 2))
        self.assertRaisesRegex(RuntimeError,
                               r"Data value type mismatch internal_calculate:double, j_k:float, k:double, x:double, v:double",
                               rho_j_k, c64(5), c32(3, 5), r64(3, 5), r64(3, 2), r64(3, 2))
        self.assertRaisesRegex(RuntimeError,
                               r"Data value type mismatch internal_calculate:double, j_k:double, k:double, x:double, v:float",
                               rho_j_k, c64(5), c64(3, 5), r64(3, 5), r64(3, 2), r32(3, 2))

    def test_rho_k_throw_if_not_correct_order(self):
        rho_k = self.get_calculate_rho_k()

        self.assertRaisesRegex(RuntimeError,
                               r"k should be of size 3 along dimension 0",
                               rho_k, cF(5), rC(3, 5), rF(3, 2))
        self.assertDoesNotRaise(rho_k, cF(5), rC(5, 3), rF(3, 2))

        self.assertRaisesRegex(RuntimeError,
                               r"x should be of size 3 along dimension 0",
                               rho_k, cF(5), rF(3, 5), rC(3, 2))
        self.assertDoesNotRaise(rho_k, cF(5), rF(3, 5), rC(2, 3))

    def test_rho_j_k_throw_if_not_correct_order(self):
        rho_j_k = self.get_calculate_rho_j_k()

        self.assertRaisesRegex(RuntimeError,
                               r"j_k should be of size 3 along dimension 0",
                               rho_j_k, cF(5), cC(3, 5), rF(3, 5), rF(3, 2), rF(3, 2))
        self.assertDoesNotRaise(rho_j_k, cF(5), cC(5, 3), rF(3, 5), rF(3, 2), rF(3, 2))

        self.assertRaisesRegex(RuntimeError,
                               r"k should be of size 3 along dimension 0",
                               rho_j_k, cF(5), cF(3, 5), rC(3, 5), rF(3, 2), rF(3, 2))
        self.assertDoesNotRaise(rho_j_k, cF(5), cF(3, 5), rC(5, 3), rF(3, 2), rF(3, 2))

        self.assertRaisesRegex(RuntimeError,
                               r"x should be of size 3 along dimension 0",
                               rho_j_k, cF(5), cF(3, 5), rF(3, 5), rC(3, 2), rF(3, 2))
        self.assertDoesNotRaise(rho_j_k, cF(5), cF(3, 5), rF(3, 5), rC(2, 3), rF(3, 2))

        self.assertRaisesRegex(RuntimeError,
                               r"v should be of size 3 along dimension 0",
                               rho_j_k, cF(5), cF(3, 5), rF(3, 5), rF(3, 2), rC(3, 2))
        self.assertDoesNotRaise(rho_j_k, cF(5), cF(3, 5), rF(3, 5), rF(3, 2), rC(2, 3))

    def test_rho_k_throw_if_not_correct_dimension(self):
        rho_k = self.get_calculate_rho_k()

        self.assertRaisesRegex(RuntimeError,
                               r"internal_calculate should be 1-dimensional",
                               rho_k, c(3, 5), r(3, 5), r(3, 2))
        self.assertRaisesRegex(RuntimeError,
                               r"k should be 2-dimensional",
                               rho_k, c(5), r(5), r(3, 2))
        self.assertRaisesRegex(RuntimeError,
                               r"x should be 2-dimensional",
                               rho_k, c(5), r(3, 5), r(3, 2, 2))

        self.assertRaisesRegex(RuntimeError,
                               r"k should be of size 3 along dimension 0",
                               rho_k, c(5), r(4, 5), r(3, 2))
        self.assertRaisesRegex(RuntimeError,
                               r"x should be of size 3 along dimension 0",
                               rho_k, c(5), r(3, 5), r(4, 2))

        self.assertRaisesRegex(RuntimeError,
                               r"k should be of size 5 along dimension 1",
                               rho_k, c(5), r(3, 7), r(3, 2))

    def test_rho_j_k_throw_if_not_correct_dimension(self):
        rho_j_k = self.get_calculate_rho_j_k()

        self.assertRaisesRegex(RuntimeError,
                               r"internal_calculate should be 1-dimensional",
                               rho_j_k, c(5, 2), c(3, 5), r(3, 5), r(3, 2), r(3, 5))
        self.assertRaisesRegex(RuntimeError,
                               r"j_k should be 2-dimensional",
                               rho_j_k, c(5), c(3, 5, 2), r(3, 5), r(3, 2), r(3, 5))
        self.assertRaisesRegex(RuntimeError,
                               r"k should be 2-dimensional",
                               rho_j_k, c(5), c(3, 5), r(3, 5, 2), r(3, 2), r(3, 5))
        self.assertRaisesRegex(RuntimeError,
                               r"x should be 2-dimensional",
                               rho_j_k, c(5), c(3, 5), r(3, 5), r(3, 2, 2), r(3, 5))
        self.assertRaisesRegex(RuntimeError,
                               r"v should be 2-dimensional",
                               rho_j_k, c(5), c(3, 5), r(3, 5), r(3, 2), r(3, 5, 2))

        self.assertRaisesRegex(RuntimeError,
                               r"j_k should be of size 3 along dimension 0",
                               rho_j_k, c(5), c(4, 5), r(3, 5), r(3, 2), r(3, 2))
        self.assertRaisesRegex(RuntimeError,
                               r"k should be of size 3 along dimension 0",
                               rho_j_k, c(5), c(3, 5), r(4, 5), r(3, 2), r(3, 2))
        self.assertRaisesRegex(RuntimeError,
                               r"x should be of size 3 along dimension 0",
                               rho_j_k, c(5), c(3, 5), r(3, 5), r(4, 2), r(3, 2))
        self.assertRaisesRegex(RuntimeError,
                               r"v should be of size 3 along dimension 0",
                               rho_j_k, c(5), c(3, 5), r(3, 5), r(3, 2), r(4, 2))

        self.assertRaisesRegex(RuntimeError,
                               r"j_k should be of size 5 along dimension 1",
                               rho_j_k, c(5), c(3, 7), r(3, 5), r(3, 2), r(3, 2))
        self.assertRaisesRegex(RuntimeError,
                               r"k should be of size 5 along dimension 1",
                               rho_j_k, c(5), c(3, 5), r(3, 7), r(3, 2), r(3, 2))
        self.assertRaisesRegex(RuntimeError,
                               r"v should be of size 2 along dimension 1",
                               rho_j_k, c(5), c(3, 5), r(3, 5), r(3, 2), r(3, 7))
