# -*- coding: utf-8 -*-
import unittest
import numpy as np

import calculate


class CalculateModuleBasicTest(unittest.TestCase):

    @staticmethod
    def get_calculate_rho_k():
        return getattr(calculate, "calculate_rho_k")

    @staticmethod
    def get_calculate_rho_j_k():
        return getattr(calculate, "calculate_rho_j_k")

    def test_corner_case_no_k_no_x(self):
        calculate_rho_k = self.get_calculate_rho_k()
        rho_k = np.zeros((0,), dtype=np.complex128)
        k = np.zeros((3, 0), dtype=np.float64)
        x = np.zeros((3, 0), dtype=np.float64)
        calculate_rho_k(rho_k, k, x)

    def test_corner_case_no_k(self):
        calculate_rho_k = self.get_calculate_rho_k()
        rho_k = np.zeros((0,), dtype=np.complex128)
        k = np.zeros((3, 0), dtype=np.float64)
        x = np.zeros((3, 1), dtype=np.float64)
        calculate_rho_k(rho_k, k, x)

    def Xtest_corner_case_no_x(self): # result will be undefined
        calculate_rho_k = self.get_calculate_rho_k()
        rho_k = np.zeros((1,), dtype=np.complex128)
        k = np.zeros((3, 1), dtype=np.float64)
        x = np.zeros((3, 0), dtype=np.float64)
        calculate_rho_k(rho_k, k, x)

        self.assertTrue(np.isnan(np.real(rho_k[0])))
        self.assertTrue(np.isnan(np.imag(rho_k[0])))

    def test_trivial_zero_k_zero_x(self):
        calculate_rho_k = self.get_calculate_rho_k()
        rho_k = np.zeros((1,), dtype=np.complex128)
        k = np.zeros((3, 1), dtype=np.float64)
        x = np.zeros((3, 1), dtype=np.float64)
        calculate_rho_k(rho_k, k, x)

        self.assertAlmostEqual(np.real(rho_k[0]), 1.0)
        self.assertAlmostEqual(np.imag(rho_k[0]), 0.0)

    def test_trivial_k_times_x_is_zero(self):
        calculate_rho_k = self.get_calculate_rho_k()
        rho_k = np.zeros((1,), dtype=np.complex128)
        k = np.zeros((3, 1), dtype=np.float64)
        x = np.zeros((3, 1), dtype=np.float64)

        k.flat[:] = (1, 0, 1)
        x.flat[:] = (0, 1, 0)

        calculate_rho_k(rho_k, k, x)

        self.assertAlmostEqual(np.real(rho_k[0]), 1.0)
        self.assertAlmostEqual(np.imag(rho_k[0]), 0.0)

    def test_trivial_k_times_x_is_half_pi(self):
        calculate_rho_k = self.get_calculate_rho_k()
        rho_k = np.zeros((1,), dtype=np.complex128)
        k = np.zeros((3, 1), dtype=np.float64)
        x = np.zeros((3, 1), dtype=np.float64)

        k.flat[:] = (0.5 * np.pi, 0, 0)
        x.flat[:] = (1, 0, 0)

        calculate_rho_k(rho_k, k, x)

        self.assertAlmostEqual(np.real(rho_k[0]), 0.0)
        self.assertAlmostEqual(np.imag(rho_k[0]), 1.0)
