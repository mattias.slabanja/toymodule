#include <catch2/catch.hpp>

#include <array_data_reference/array_data_reference.hpp>
#include <internal_calculate/implementation_type.hpp>
#include <internal_calculate/rho_k.hpp>
#include <internal_calculate/impl/naive_rho_k.hpp>
#include <internal_calculate/impl/openacc_rho_k.hpp>
#include <internal_calculate/impl/openmp_rho_k.hpp>

#include <array>
#include <complex>
#include <vector>
#include <numbers>

namespace c = calculate;
namespace ci = calculate::impl;

template<typename T>
bool is_implemented(){
    T i;
    return i.implemented();
}

TEMPLATE_TEST_CASE("Create calculator", "[]", float, double, long double)
{
    using T = TestType;
    using C = std::complex<T>;

    const auto data_value_type = c::data_value_type::FromType<T>::data_element_type.first;

    const T margin = std::numeric_limits<T>::epsilon();

    std::vector<T> xs(3, 0);
    std::vector<T> ks(3, 0);
    std::vector<C> rho_ks(1, {0, 0});

    auto xs_ref = c::ArrayDataReference(xs, "xs");
    auto ks_ref = c::ArrayDataReference(ks, "ks");
    auto rho_ks_ref = c::ArrayDataReference(rho_ks, "rho_ks");

    xs = {0, 0, 0};
    ks = {0, 0, 0};

    SECTION("Can create and perform simple call to Naive")
    {
        using ImplType = ci::NaiveRhoK<T>;
        if (is_implemented<ImplType>()) {
            auto naive_rho_k =
              c::RhoK::implementation(c::ImplementationType::Naive, data_value_type);
            REQUIRE(naive_rho_k);
            REQUIRE(dynamic_cast<ImplType*>(naive_rho_k.get()) != nullptr);

            naive_rho_k->calculate(rho_ks_ref, ks_ref, xs_ref);
            REQUIRE(rho_ks[0].real() == Approx(1.0).margin(margin));
            REQUIRE(rho_ks[0].imag() == Approx(0.0).margin(margin));
        } else {
            WARN("NaiveRhoK not implemented");
        }
    }

    SECTION("Can create and perform simple call to OpenMP")
    {
        using ImplType = ci::OpenMPRhoK<T>;
        if (is_implemented<ImplType>()) {
            auto openmp_rho_k =
              c::RhoK::implementation(c::ImplementationType::OpenMP, data_value_type);
            REQUIRE(openmp_rho_k);
            REQUIRE(dynamic_cast<ImplType*>(openmp_rho_k.get()) != nullptr);

            openmp_rho_k->calculate(rho_ks_ref, ks_ref, xs_ref);
            REQUIRE(rho_ks[0].real() == Approx(1.0).margin(margin));
            REQUIRE(rho_ks[0].imag() == Approx(0.0).margin(margin));
        } else {
            WARN("OpenMPRhoK not implemented");
        }
    }

    SECTION("Can create and perform simple call to OpenACC")
    {
        using ImplType = ci::OpenACCRhoK<T>;
        if (is_implemented<ImplType>()) {
            auto openacc_rho_k =
              c::RhoK::implementation(c::ImplementationType::OpenACC, data_value_type);
            REQUIRE(openacc_rho_k);
            REQUIRE(dynamic_cast<ImplType*>(openacc_rho_k.get()) != nullptr);

            openacc_rho_k->calculate(rho_ks_ref, ks_ref, xs_ref);
            REQUIRE(rho_ks[0].real() == Approx(1.0).margin(margin));
            REQUIRE(rho_ks[0].imag() == Approx(0.0).margin(margin));
        } else {
            WARN("OpenACCRhoK not implemented");
        }
    }
}