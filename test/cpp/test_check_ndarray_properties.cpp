#include <catch2/catch.hpp>
#define MY_REQUIRE_THROWS_AS_WITH(expr, type, message_match)                                       \
    REQUIRE_THROWS_AS(expr, type);                                                                 \
    REQUIRE_THROWS_WITH(expr, message_match)

#include <array_data_reference/array_data_reference.hpp>
#include <python_module/check_ndarray_properties.hpp>
#include <python_module/module_exception.hpp>

#include <boost/python/numpy.hpp>
#include <boost/python/object.hpp>
#include <complex>


namespace c = calculate;
namespace np = boost::python::numpy;
namespace py = boost::python;

using Catch::Matchers::Matches;
using Catch::Matchers::Contains;

TEST_CASE("CheckNDArrayProperties", "[check_ndarray_properties]") {
    Py_Initialize();
    boost::python::numpy::initialize();

    SECTION("1D double, shape (3,)")
    {
        using Type = double;
        constexpr std::size_t size = 3;
        constexpr std::size_t dims = 1;
        constexpr auto printable_name = "test_array";

        np::ndarray a = np::zeros(py::make_tuple(size), np::dtype::get_builtin<Type>());
        c::ArrayDataReference a_ref(a, printable_name);

        SECTION("Number of dimensions")
        {
            REQUIRE_NOTHROW(c::check_get_shape<dims>(a_ref));
            REQUIRE_NOTHROW(c::check_get_shape<dims>(a_ref, size));

            MY_REQUIRE_THROWS_AS_WITH(c::check_get_shape<dims - 1>(a_ref),
                                      c::CalculateException,
                                      "test_array should be 0-dimensional (is a 1-dimensional array)");
            MY_REQUIRE_THROWS_AS_WITH(c::check_get_shape<dims + 1>(a_ref),
                                      c::CalculateException,
                                      "test_array should be 2-dimensional (is a 1-dimensional array)");
        }

        SECTION("Dimension sizes")
        {
            const auto get_shape_result_0 = c::check_get_shape<dims>(a_ref);
            const auto get_shape_result_1 = c::check_get_shape<dims>(a_ref, size);
            REQUIRE(std::size(get_shape_result_0) == 1);
            REQUIRE(get_shape_result_0[0] == size);
            REQUIRE(std::size(get_shape_result_1) == 0);
        }
    }

    SECTION("1D std::complex<double>, shape (7,)")
    {
        using Type = std::complex<double>;
        constexpr std::size_t size = 7;
        constexpr std::size_t dims = 1;
        constexpr auto printable_name = "complex_test_array";

        np::ndarray a = np::zeros(py::make_tuple(size), np::dtype::get_builtin<Type>());
        c::ArrayDataReference a_ref(a, printable_name);

        SECTION("Number of dimensions")
        {
            REQUIRE_NOTHROW(c::check_get_shape<dims>(a_ref));
            REQUIRE_NOTHROW(c::check_get_shape<dims>(a_ref, size));
        }

        SECTION("Dimension sizes")
        {
            const auto get_shape_result_0 = c::check_get_shape<dims>(a_ref);
            const auto get_shape_result_1 = c::check_get_shape<dims>(a_ref, size);
            REQUIRE(std::size(get_shape_result_0) == 1);
            REQUIRE(get_shape_result_0[0] == size);
            REQUIRE(std::size(get_shape_result_1) == 0);
        }
    }

}


