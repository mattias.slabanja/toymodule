#include <catch2/catch.hpp>

#include <array_data_reference/array_data_reference.hpp>
#include <internal_calculate/impl/naive_rho_k.hpp>
#include <internal_calculate/impl/openmp_rho_k.hpp>
#include <internal_calculate/impl/openacc_rho_k.hpp>

#include <complex>
#include <memory>
#include <numbers>
#include <random>
#include <vector>


namespace c = calculate;
namespace ci = calculate::impl;

template<typename T, std::size_t N, std::size_t Seed>
c::ArrayDataReference&
get_static_data()
{
    static std::vector<T> data(N, T{});
    static c::ArrayDataReference data_ref = c::ArrayDataReference(data, "data");
    static bool initialized = false;

    if (initialized)
        return data_ref;

    std::mt19937 rng;
    rng.seed(Seed);
    std::uniform_real_distribution<long double> unif(0.0, 1.0);
    for (std::size_t ix; ix < N; ++ix)
        data[ix] = static_cast<T>(unif(rng));

    return data_ref;
}

static constexpr std::size_t Nx = 3000;
static constexpr std::size_t Nk = 2000;
static constexpr std::size_t small_Nx = 10;
static constexpr std::size_t small_Nk = 10;

template <typename T, std::size_t DataSize, std::size_t ...Seeds>
std::vector<c::ArrayDataReference>
create_data_vector(){
    std::vector<c::ArrayDataReference> result;
    result.reserve(sizeof...(Seeds));
    (result.push_back(get_static_data<T, DataSize, Seeds>()), ...);
    return result;
}

template<typename T>
std::vector<c::ArrayDataReference> global_xs_refs;
template<typename T>
std::vector<c::ArrayDataReference> global_ks_refs;

template<>
auto global_xs_refs<float> = create_data_vector<float, 3 * Nx, 1, 2, 3, 4, 5>();
template<>
auto global_xs_refs<double> = create_data_vector<double, 3 * Nx, 1, 2, 3, 4, 5>();
template<>
auto global_xs_refs<long double> = create_data_vector<long double, 3 * Nx, 1, 2, 3, 4, 5>();

template<>
auto global_ks_refs<float> = create_data_vector<float, 3 * Nk, 101, 102, 103, 104, 105>();
template<>
auto global_ks_refs<double> = create_data_vector<double, 3 * Nk, 101, 102, 103, 104, 105>();
template<>
auto global_ks_refs<long double> = create_data_vector<long double, 3 * Nk, 101, 102, 103, 104, 105>();


TEMPLATE_PRODUCT_TEST_CASE("rho_k", "[]",
                           (ci::NaiveRhoK, ci::OpenMPRhoK, ci::OpenACCRhoK),
                           (float, double, long double))
{
    using RhoK = TestType;
    using T = typename RhoK::type;
    using C = std::complex<T>;

    auto rho_k = std::make_unique<RhoK>();
    if (rho_k->implemented()) {

        std::vector<C> rho_ks(Nk, {0, 0});
        auto rho_ks_ref = c::ArrayDataReference(rho_ks, "rho_ks");
        auto small_xs_ref = get_static_data<T, 3 * small_Nx, 1>();
        auto small_ks_ref = get_static_data<T, 3 * small_Nk, 1>();

        BENCHMARK("small number of x-points", i)
        {
            std::size_t ix = i % global_ks_refs<T>.size();
            rho_k->calculate(rho_ks_ref, global_ks_refs<T>[ix], small_xs_ref);
        };

        BENCHMARK("small number of k-points", i)
        {
            std::size_t ix = i % global_xs_refs<T>.size();
            rho_k->calculate(rho_ks_ref, small_ks_ref, global_xs_refs<T>[ix]);
        };

        BENCHMARK("perform somewhat realistically sized calculation", i)
        {
            std::size_t ix = i % global_ks_refs<T>.size();
            rho_k->calculate(rho_ks_ref, global_ks_refs<T>[ix], global_xs_refs<T>[ix]);
        };
    }
}

