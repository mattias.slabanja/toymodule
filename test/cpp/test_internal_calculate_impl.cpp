#include <catch2/catch.hpp>

#include <array_data_reference/array_data_reference.hpp>
#include <internal_calculate/impl/naive_rho_k.hpp>
#include <internal_calculate/impl/openmp_rho_k.hpp>
#include <internal_calculate/impl/openacc_rho_k.hpp>

#include <array>
#include <complex>
#include <vector>
#include <memory>
#include <numbers>

#include <iostream>

namespace c = calculate;
namespace ci = calculate::impl;


TEMPLATE_PRODUCT_TEST_CASE("Test trivial internal calculate impl", "[]",
                           (ci::NaiveRhoK, ci::OpenMPRhoK, ci::OpenACCRhoK),
                           (float, double, long double))
{
    using RhoK = TestType;
    using T = typename RhoK::type;
    using C = std::complex<T>;

    auto rho_k = std::make_unique<RhoK>();
    if (rho_k->implemented()) {

        const T pi = std::numbers::pi_v<T>;
        const T epsilon = std::numeric_limits<T>::epsilon();
        //    const T margin = epsilon;
        const T margin = std::is_same_v<T, long double> ? 1.5 * epsilon : epsilon;

        std::vector<T> xs(3, 0);
        std::vector<T> ks(3, 0);
        std::vector<C> rho_ks(1, {0, 0});

        auto xs_ref = c::ArrayDataReference(xs, "xs");
        auto ks_ref = c::ArrayDataReference(ks, "ks");
        auto rho_ks_ref = c::ArrayDataReference(rho_ks, "rho_ks");

        SECTION("Can perform simple calls")
        {
            xs = {0, 0, 0};
            ks = {0, 0, 0};
            rho_k->calculate(rho_ks_ref, ks_ref, xs_ref);
            REQUIRE(rho_ks[0].real() == Approx(1.0).margin(margin));
            REQUIRE(rho_ks[0].imag() == Approx(0.0).margin(margin));

            xs = {0, 0, 0};
            ks = {1, 1, 1};
            rho_k->calculate(rho_ks_ref, ks_ref, xs_ref);
            REQUIRE(rho_ks[0].real() == Approx(1.0).margin(margin));
            REQUIRE(rho_ks[0].imag() == Approx(0.0).margin(margin));

            xs = {1, 1, 1};
            ks = {0, 0, 0};
            rho_k->calculate(rho_ks_ref, ks_ref, xs_ref);
            REQUIRE(rho_ks[0].real() == Approx(1.0).margin(margin));
            REQUIRE(rho_ks[0].imag() == Approx(0.0).margin(margin));

            xs = {0, 0, 0.5};
            ks = {0, 0, pi};
            rho_k->calculate(rho_ks_ref, ks_ref, xs_ref);
            REQUIRE(rho_ks[0].real() == Approx(0.0).margin(margin));
            REQUIRE(rho_ks[0].imag() == Approx(1.0).margin(margin));

            xs = {T{1} / 6, T{1} / 6, T{1} / 6};
            ks = {pi, pi, pi};
            rho_k->calculate(rho_ks_ref, ks_ref, xs_ref);
            REQUIRE(rho_ks[0].real() == Approx(0.0).margin(margin));
            REQUIRE(rho_ks[0].imag() == Approx(1.0).margin(margin));

            xs = {0.25, 0.25, 0.0};
            ks = {pi, pi, pi};
            rho_k->calculate(rho_ks_ref, ks_ref, xs_ref);
            REQUIRE(rho_ks[0].real() == Approx(0.0).margin(margin));
            REQUIRE(rho_ks[0].imag() == Approx(1.0).margin(margin));
        }
    }
}




TEMPLATE_PRODUCT_TEST_CASE("Test simple internal calculate impl", "[]",
                           (ci::NaiveRhoK, ci::OpenMPRhoK, ci::OpenACCRhoK),
                           (float, double, long double))
{
    using RhoK = TestType;
    using T = typename RhoK::type;
    using C = std::complex<T>;

    auto rho_k = std::make_unique<RhoK>();
    if (rho_k->implemented()) {


        const T epsilon = std::numeric_limits<T>::epsilon();
        const T margin = epsilon;

        const std::size_t Nx = 3;
        const std::size_t Nk = 2;

        const T pi = std::numbers::pi_v<T>;
        const auto sqrtNx = std::sqrt(T{Nx});

        std::vector<T> xs(Nx * 3, 0);
        std::vector<T> ks(Nk * 3, 0);
        std::vector<C> rho_ks(Nk, {0, 0});

        auto xs_ref = c::ArrayDataReference(xs, "xs");
        auto ks_ref = c::ArrayDataReference(ks, "ks");
        auto rho_ks_ref = c::ArrayDataReference(rho_ks, "rho_ks");

        SECTION("Normalization over x:s")
        {
            xs = {0, 0, 0,
                  0, 0, 0,
                  0, 0, 0};
            ks = {0.0, 0.0, 0.0,
                  1.0, 1.0, 1.0};
            rho_k->calculate(rho_ks_ref, ks_ref, xs_ref);
            REQUIRE(rho_ks[0].real() == Approx(sqrtNx).margin(margin));
            REQUIRE(rho_ks[0].imag() == Approx(0.0).margin(margin));
            REQUIRE(rho_ks[1].real() == Approx(sqrtNx).margin(margin));
            REQUIRE(rho_ks[1].imag() == Approx(0.0).margin(margin));
        }

    }
}
