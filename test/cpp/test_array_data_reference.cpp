#include <catch2/catch.hpp>

#include <array_data_reference/array_data_reference.hpp>

#include <boost/python/numpy.hpp>
#include <boost/python/object.hpp>
#include <complex>


namespace c = calculate;
namespace dvt = calculate::data_value_type;
namespace np = boost::python::numpy;
namespace py = boost::python;

constexpr std::size_t One = 1;

TEST_CASE("ArrayDataReference, basic usage", "[ArrayDataReference]") {
    Py_Initialize();
    boost::python::numpy::initialize();

    SECTION("1D double, shape (3,)")
    {
        using Type = double;
        constexpr std::size_t size = 3;
        constexpr std::size_t dims = 1;
        constexpr auto printable_name = "test_array";

        using OtherValueType = float;
        using OtherNumberType = std::complex<Type>;

        np::ndarray a = np::zeros(py::make_tuple(size), np::dtype::get_builtin<Type>());
        c::ArrayDataReference a_ref(a, printable_name);

        REQUIRE(a_ref.data_value_type() == c::DataValueType::Double);
        REQUIRE(a_ref.data_number_type() == c::DataNumberType::Real);
        REQUIRE(a_ref.printable_name() == printable_name);
        REQUIRE(a_ref.number_of_dimensions() == dims);
        REQUIRE(*a_ref.shape(0) == size);
        REQUIRE(a_ref.shape(1) == std::nullopt);
        REQUIRE(a_ref.as_span<Type>()->size() == size);
        REQUIRE(a_ref.as_span<Type[size]>()->size() == One);
        REQUIRE(std::size((*a_ref.as_span<Type[size]>())[0]) == size);
        REQUIRE(!a_ref.as_span<OtherValueType>());
        REQUIRE(!a_ref.as_span<OtherNumberType>());
        REQUIRE(!a_ref.as_span<Type[size + 1]>());
    }

    SECTION("1D std::complex<double>, shape (7,)")
    {
        using Type = std::complex<double>;
        constexpr std::size_t size = 7;
        constexpr std::size_t dims = 1;
        constexpr auto printable_name = "complex_test_array";

        using OtherValueType = std::complex<float>;
        using OtherNumberType = double;

        np::ndarray a = np::zeros(py::make_tuple(size), np::dtype::get_builtin<Type>());
        c::ArrayDataReference a_ref(a, printable_name);

        REQUIRE(a_ref.data_value_type() == c::DataValueType::Double);
        REQUIRE(a_ref.data_number_type() == c::DataNumberType::Complex);
        REQUIRE(a_ref.printable_name() == printable_name);
        REQUIRE(a_ref.number_of_dimensions() == dims);
        REQUIRE(*a_ref.shape(0) == size);
        REQUIRE(a_ref.shape(1) == std::nullopt);
        REQUIRE(a_ref.as_span<Type>()->size() == size);
        REQUIRE(a_ref.as_span<Type[size]>()->size() == One);
        REQUIRE(std::size((*a_ref.as_span<Type[size]>())[0]) == size);
        REQUIRE(!a_ref.as_span<OtherValueType>());
        REQUIRE(!a_ref.as_span<OtherNumberType>());
        REQUIRE(!a_ref.as_span<Type[size + 1]>());
    }

    SECTION("2D float, shape (3,15)")
    {
        using Type = float;
        constexpr std::size_t size_major = 3;
        constexpr std::size_t size_minor = 15;
        constexpr std::size_t n_elements = size_major * size_minor;
        constexpr std::size_t dims = 2;
        constexpr auto printable_name = "ndarray";

        using OtherValueType = double;
        using OtherNumberType = std::complex<Type>;

        np::ndarray a =  // C-contiguous arrays are row-major
          np::zeros(py::make_tuple(size_minor, size_major), np::dtype::get_builtin<Type>());
        c::ArrayDataReference a_ref(a, printable_name);

        REQUIRE(a_ref.data_value_type() == dvt::FromType<Type>::data_element_type.first);
        REQUIRE(a_ref.data_number_type() == dvt::FromType<Type>::data_element_type.second);
        REQUIRE(a_ref.printable_name() == printable_name);
        REQUIRE(a_ref.number_of_dimensions() == dims);
        REQUIRE(*a_ref.shape(0) == size_major);
        REQUIRE(*a_ref.shape(1) == size_minor);
        REQUIRE(a_ref.shape(2) == std::nullopt);
        REQUIRE(a_ref.as_span<Type>()->size() == n_elements);
        REQUIRE(a_ref.as_span<Type[size_major]>()->size() == size_minor);
        REQUIRE(std::size((*a_ref.as_span<Type[size_major]>())[0]) == size_major);
        REQUIRE(a_ref.as_span<Type[size_minor]>()->size() == size_major);
        REQUIRE(std::size((*a_ref.as_span<Type[size_minor]>())[0]) == size_minor);
        REQUIRE(!a_ref.as_span<OtherValueType>());
        REQUIRE(!a_ref.as_span<OtherNumberType>());
        REQUIRE(!a_ref.as_span<Type[size_major + 1]>());
    }

    SECTION("std::complex<float> hosted by std::vector")
    {
        using Type = std::complex<float>;
        constexpr std::size_t size_major = 3;
        constexpr std::size_t size_minor = 15;
        constexpr std::size_t n_elements = size_major * size_minor;
        constexpr std::size_t dims = 1;
        constexpr auto printable_name = "vector";

        using OtherValueType = double;
        using OtherNumberType = float;

        std::vector<Type> v(n_elements, Type{});
        c::ArrayDataReference a_ref(v, printable_name);

        SECTION("Properties of ArrayDataReference")
        {
            REQUIRE(a_ref.data_value_type() == dvt::FromType<Type>::data_element_type.first);
            REQUIRE(a_ref.data_number_type() == dvt::FromType<Type>::data_element_type.second);
            REQUIRE(a_ref.printable_name() == printable_name);
            REQUIRE(a_ref.number_of_dimensions() == dims);
            REQUIRE(*a_ref.shape(0) == n_elements);
            REQUIRE(a_ref.shape(1) == std::nullopt);
            REQUIRE(a_ref.as_span<Type>()->size() == n_elements);
            REQUIRE(a_ref.as_span<Type[size_major]>()->size() == size_minor);
            REQUIRE(std::size((*a_ref.as_span<Type[size_major]>())[0]) == size_major);
            REQUIRE(a_ref.as_span<Type[size_minor]>()->size() == size_major);
            REQUIRE(std::size((*a_ref.as_span<Type[size_minor]>())[0]) == size_minor);
            REQUIRE(!a_ref.as_span<OtherValueType>());
            REQUIRE(!a_ref.as_span<OtherNumberType>());
            REQUIRE(!a_ref.as_span<Type[size_major + 1]>());
        }

        SECTION("Write to data via 1D reference")
        {
            auto s = *a_ref.as_span<Type>();
            for(std::size_t ix = 0; ix < s.size(); ++ix)
                s[ix] = static_cast<Type>(1+ix);
            for(std::size_t ix = 0; ix < v.size(); ++ix)
                REQUIRE(v[ix] == static_cast<Type>(1+ix));
        }

        SECTION("Write to data via 2D reference")
        {
            auto s = *a_ref.as_span<Type[size_major]>();
            for (std::size_t minor_ix = 0; minor_ix < s.size(); ++minor_ix) {
                for (std::size_t major_ix = 0; major_ix < size_major; ++major_ix) {
                    s[minor_ix][major_ix] = static_cast<Type>(2 + major_ix + size_major * minor_ix);
                }
            }
            for (std::size_t ix = 0; ix < v.size(); ++ix) {
                REQUIRE(v[ix] == static_cast<Type>(ix + 2));
            }
        }

    }

    SECTION("long double hosted by std::vector")
    {
        using Type = long double;
        constexpr std::size_t size_major = 7;
        constexpr std::size_t size_minor = 1;
        constexpr std::size_t n_elements = size_major * size_minor;
        constexpr std::size_t dims = 1;
        constexpr auto printable_name = "vector";

        using OtherValueType = double;
        using OtherNumberType = float;

        std::vector<Type> v(n_elements, Type{});
        c::ArrayDataReference a_ref(v, printable_name);

        SECTION("Properties of ArrayDataReference")
        {
            REQUIRE(a_ref.data_value_type() == dvt::FromType<Type>::data_element_type.first);
            REQUIRE(a_ref.data_number_type() == dvt::FromType<Type>::data_element_type.second);
            REQUIRE(a_ref.printable_name() == printable_name);
            REQUIRE(a_ref.number_of_dimensions() == dims);
            REQUIRE(*a_ref.shape(0) == n_elements);
            REQUIRE(a_ref.shape(1) == std::nullopt);
            REQUIRE(a_ref.as_span<Type>()->size() == n_elements);
            REQUIRE(a_ref.as_span<Type[size_major]>()->size() == size_minor);
            REQUIRE(std::size((*a_ref.as_span<Type[size_major]>())[0]) == size_major);
            REQUIRE(a_ref.as_span<Type[size_minor]>()->size() == size_major);
            REQUIRE(std::size((*a_ref.as_span<Type[size_minor]>())[0]) == size_minor);
            REQUIRE(!a_ref.as_span<OtherValueType>());
            REQUIRE(!a_ref.as_span<OtherNumberType>());
            REQUIRE(!a_ref.as_span<Type[size_major + 1]>());
        }

        SECTION("Write to data via simple span")
        {
            auto s = *a_ref.as_span<Type>();
            for (std::size_t ix = 0; ix < s.size(); ++ix)
                s[ix] = static_cast<Type>(1 + ix);
            for (std::size_t ix = 0; ix < v.size(); ++ix)
                REQUIRE(v[ix] == static_cast<Type>(1 + ix));
        }

        SECTION("Write to data via span of arrays")
        {
            auto s = *a_ref.as_span<Type[size_major]>();
            for (std::size_t minor_ix = 0; minor_ix < s.size(); ++minor_ix) {
                for (std::size_t major_ix = 0; major_ix < size_major; ++major_ix) {
                    s[minor_ix][major_ix] = static_cast<Type>(2 + major_ix + size_major * minor_ix);
                }
            }
            for (std::size_t ix = 0; ix < v.size(); ++ix) {
                REQUIRE(v[ix] == static_cast<Type>(ix + 2));
            }
        }
    }

    SECTION("Accessing via multi-dimensional array")
    {
        using Type = double;
        constexpr std::size_t dim_1 = 3;
        constexpr std::size_t dim_2 = 5;
        constexpr std::size_t dim_3 = 7;
        constexpr std::size_t dim_4 = 11;
        constexpr std::size_t n_elements = dim_1 * dim_2 * dim_3 * dim_4;
        constexpr auto printable_name = "vector";

        std::vector<Type> v(n_elements, Type{});
        c::ArrayDataReference a_ref(v, printable_name);

        auto s_2d_1 = *a_ref.as_span<Type[dim_1][dim_2]>();
        REQUIRE(s_2d_1.size() == n_elements / (dim_1 * dim_2));

        auto s_2d_2 = *a_ref.as_span<Type[dim_2][dim_4]>();
        REQUIRE(s_2d_2.size() == n_elements / (dim_2 * dim_4));

        auto s_3d_1 = *a_ref.as_span<Type[dim_1][dim_3][dim_4]>();
        REQUIRE(s_3d_1.size() == dim_2);

        auto s_4d_1 = *a_ref.as_span<Type[dim_4][dim_2][dim_1][dim_3]>();
        REQUIRE(s_4d_1.size() == 1);
    }

}


