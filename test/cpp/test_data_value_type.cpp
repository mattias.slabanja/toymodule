#include <catch2/catch.hpp>

#include <array_data_reference/data_value_type.hpp>

#include <complex>


namespace c = calculate;
namespace dvt = calculate::data_value_type;

TEST_CASE("DataValueType", "[DataValueType]") {
    SECTION("FromType")
    {
        REQUIRE(dvt::FromType<float>::data_element_type
                == c::DataElementType {c::DataValueType::Float, c::DataNumberType::Real});

        REQUIRE(dvt::FromType<double>::data_element_type
                == c::DataElementType {c::DataValueType::Double, c::DataNumberType::Real});

        REQUIRE(dvt::FromType<long double>::data_element_type
                == c::DataElementType {c::DataValueType::LongDouble, c::DataNumberType::Real});

        REQUIRE(dvt::FromType<std::complex<float>>::data_element_type
                == c::DataElementType {c::DataValueType::Float, c::DataNumberType::Complex});

        REQUIRE(dvt::FromType<std::complex<double>>::data_element_type
                == c::DataElementType {c::DataValueType::Double, c::DataNumberType::Complex});

        REQUIRE(dvt::FromType<std::complex<long double>>::data_element_type
                == c::DataElementType {c::DataValueType::LongDouble, c::DataNumberType::Complex});

        REQUIRE(dvt::FromType<int>::data_element_type
                == c::DataElementType {c::DataValueType::Unsupported, c::DataNumberType::Undefined});
    }

}


