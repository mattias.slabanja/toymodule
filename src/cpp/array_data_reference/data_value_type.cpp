#include "data_value_type.hpp"

#include <boost/python/numpy/ndarray.hpp>

namespace np = boost::python::numpy;

namespace calculate {

std::string_view
printable_name(DataValueType data_value_type)
{
    switch (data_value_type) {
        case (DataValueType::Float): return "float";
        case (DataValueType::Double): return "double";
        case (DataValueType::LongDouble): return "long double";
        case (DataValueType::Unsupported): return "<unsupported type>";
        default: return "";
    }
}

}


namespace calculate::data_value_type{

DataElementType
value_element_type(const boost::python::numpy::ndarray& array)
{
    const np::dtype array_dtype = array.get_dtype();
    if (array_dtype == np::dtype::get_builtin<float>()) {
        return FromType<float>::data_element_type;
    } else if (array_dtype == np::dtype::get_builtin<std::complex<float>>()) {
        return FromType<std::complex<float>>::data_element_type;
    } else if (array_dtype == np::dtype::get_builtin<double>()) {
        return FromType<double>::data_element_type;
    } else if (array_dtype == np::dtype::get_builtin<std::complex<double>>()) {
        return FromType<std::complex<double>>::data_element_type;
    } else if (array_dtype == np::dtype::get_builtin<long double>()) {
        return FromType<long double>::data_element_type;
    } else if (array_dtype == np::dtype::get_builtin<std::complex<long double>>()) {
        return FromType<std::complex<long double>>::data_element_type;
    }
    return FromType<void>::data_element_type;
}


}