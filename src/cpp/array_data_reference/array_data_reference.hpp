#pragma once

#include <array_data_reference/data_value_type.hpp>
#include <array_data_reference/type_utils.hpp>

#include <complex>
#include <optional>
#include <span>
#include <string>
#include <type_traits>
#include <utility>
#include <vector>


namespace boost::python::numpy {
class ndarray;
};

namespace calculate {


class ArrayDataReference
{
  public:
    ArrayDataReference(const boost::python::numpy::ndarray& array, std::string printable_name);

    template<typename T, typename = std::enable_if_t<is_complex_or_floating_point_v<T>>>
    inline ArrayDataReference(std::vector<T>& vector, std::string printable_name)
      : printable_name_m(std::move(printable_name))
      , data_element_type_m(data_value_type::FromType<T>::data_element_type)
      , shape_m(1, vector.size())
      , n_elements_m(vector.size())
      , raw_data_m(vector.data())
    {}

    DataValueType data_value_type() const;
    std::string_view printable_name() const;
    std::size_t number_of_dimensions() const;
    std::optional<std::size_t> shape(std::size_t dimension) const;
    DataNumberType data_number_type() const;

    template<typename T, std::enable_if_t<is_complex_or_floating_point_v<T>, int> = 0>
    inline std::optional<std::span<T>> as_span() const
    {
        if (element_has_type<T>())
            return {std::span<T>(reinterpret_cast<T*>(raw_data_m), n_elements_m)};

        return std::nullopt;
    }

    template<typename TA,
             std::enable_if_t<std::is_array_v<TA>, int> = 0,
             typename T = std::remove_all_extents_t<TA>,
             std::enable_if_t<is_complex_or_floating_point_v<T>, int> = 0,
             std::size_t MajorSize = total_extent_v<TA>>
    inline std::optional<std::span<TA>> as_span() const
    {
        if (element_has_type<T>()) {
            const std::optional<std::size_t> n_arrays = minor_size<MajorSize>();
            if (n_arrays) {
                return {std::span<TA>(reinterpret_cast<TA*>(raw_data_m), *n_arrays)};
            }
        }
        return std::nullopt;
    }

  private:
    template<typename T>
    inline bool element_has_type() const
    {
        return data_value_type::FromType<T>::data_element_type == data_element_type_m;
    }

    template<std::size_t MajorSize>
    inline std::optional<std::size_t> minor_size() const
    {
        const auto quotient = n_elements_m / MajorSize;
        const auto remainder = n_elements_m % MajorSize;
        if (remainder == 0)
            return {quotient};
        return std::nullopt;
    }

    std::string printable_name_m;
    DataElementType data_element_type_m;
    std::vector<std::size_t> shape_m;
    std::size_t n_elements_m;
    void* raw_data_m;
};

}