#pragma once

#include <complex>
#include <string_view>
#include <tuple>

namespace boost::python::numpy {
class ndarray;
};

namespace calculate {

enum class DataValueType
{
    Float,
    Double,
    LongDouble,
    Unsupported
};

std::string_view
printable_name(DataValueType data_value_type);

enum class DataNumberType
{
    Real,
    Complex,
    Undefined
};

using DataElementType = std::pair<DataValueType, DataNumberType>;

}

namespace calculate::data_value_type{

DataElementType
value_element_type(const boost::python::numpy::ndarray& array);


template<typename T>
struct FromType
{
    static constexpr DataElementType data_element_type = {DataValueType::Unsupported, DataNumberType::Undefined};
};

template<>
struct FromType<float>
{
    static constexpr DataElementType data_element_type = {DataValueType::Float, DataNumberType::Real};
};

template<>
struct FromType<std::complex<float>>
{
static constexpr DataElementType data_element_type = {DataValueType::Float, DataNumberType::Complex};
};

template<>
struct FromType<double>
{
    static constexpr DataElementType data_element_type = {DataValueType::Double, DataNumberType::Real};
};

template<>
struct FromType<std::complex<double>>
{
static constexpr DataElementType data_element_type = {DataValueType::Double, DataNumberType::Complex};
};

template<>
struct FromType<long double>
{
    static constexpr DataElementType data_element_type = {DataValueType::LongDouble, DataNumberType::Real};
};

template<>
struct FromType<std::complex<long double>>
{
static constexpr DataElementType data_element_type = {DataValueType::LongDouble, DataNumberType::Complex};
};

}