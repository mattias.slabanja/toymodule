#include "array_data_reference.hpp"

#include <boost/python/numpy/ndarray.hpp>
#include <complex>
#include <functional>
#include <numeric>

namespace np = boost::python::numpy;

namespace calculate {
namespace {

std::vector<std::size_t>
major_dimension_first_shape(const np::ndarray& array)
{
    const auto bitflags = array.get_flags();
    const auto raw_shape = std::span<const Py_intptr_t>(array.get_shape(), array.get_nd());
    if (bitflags & np::ndarray::bitflag::F_CONTIGUOUS) {
        return {raw_shape.begin(), raw_shape.end()};
    } else if (bitflags & np::ndarray::bitflag::C_CONTIGUOUS) {
        return {raw_shape.rbegin(), raw_shape.rend()};
    }
    return {};
}

}

using namespace data_value_type;

ArrayDataReference::ArrayDataReference(const np::ndarray& array,
                                       std::string printable_name)
  : printable_name_m(std::move(printable_name))
  , data_element_type_m(value_element_type(array))
  , shape_m(major_dimension_first_shape(array))
  , n_elements_m(std::reduce(shape_m.begin(), shape_m.end(), 1, std::multiplies<>()))
  , raw_data_m(array.get_data())
{}


DataValueType
ArrayDataReference::data_value_type() const
{
    return data_element_type_m.first;
}

std::string_view
ArrayDataReference::printable_name() const
{
    return printable_name_m;
}

std::size_t
ArrayDataReference::number_of_dimensions() const
{
    return shape_m.size();
}

std::optional<std::size_t>
ArrayDataReference::shape(std::size_t dimension) const
{
    if (dimension < number_of_dimensions())
        return {shape_m[dimension]};
    return std::nullopt;
}

DataNumberType
ArrayDataReference::data_number_type() const
{
    return data_element_type_m.second;
}

}
