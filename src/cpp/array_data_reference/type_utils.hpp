#pragma once

#include <type_traits>


namespace calculate {

template<typename T>
struct total_extent_t
{
    constexpr static std::size_t value = 1;
};

template<typename T, std::size_t N>
struct total_extent_t<T[N]>
{
    constexpr static std::size_t value = N * total_extent_t<T>::value;
};

template<typename T>
inline static constexpr std::size_t total_extent_v = total_extent_t<T>::value;

template<typename T>
struct is_complex_or_floating_point : std::is_floating_point<T>
{};

template<typename T>
struct is_complex_or_floating_point<std::complex<T>> : std::is_floating_point<T>
{};

template<typename T>
inline static constexpr bool is_complex_or_floating_point_v =
  is_complex_or_floating_point<T>::value;

}