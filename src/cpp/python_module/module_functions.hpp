#pragma once

namespace boost::python::numpy {
class ndarray;
}

namespace calculate {

void
calculate_rho_k(boost::python::numpy::ndarray& rho_k,
                const boost::python::numpy::ndarray& k,
                const boost::python::numpy::ndarray& x);

void
calculate_rho_j_k(boost::python::numpy::ndarray& rho_k,
                  boost::python::numpy::ndarray& j_k,
                  const boost::python::numpy::ndarray& k,
                  const boost::python::numpy::ndarray& x,
                  const boost::python::numpy::ndarray& v);

}
