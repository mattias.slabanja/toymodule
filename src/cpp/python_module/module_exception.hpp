#pragma once

#include <exception>
#include <string>

namespace calculate {

class CalculateException : public std::exception
{
  public:
    explicit CalculateException(std::string message);
    virtual char const* what() const noexcept;

  private:
    std::string message_m;
};

void
translate_exception(const CalculateException& exception);

}
