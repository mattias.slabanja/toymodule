#include <python_module/module_exception.hpp>
#include <python_module/module_functions.hpp>

#include <boost/python/def.hpp>
#include <boost/python/exception_translator.hpp>
#include <boost/python/module.hpp>
#include <boost/python/numpy.hpp>

namespace py = boost::python;
namespace np = boost::python::numpy;

BOOST_PYTHON_MODULE(calculate)
{
    Py_Initialize();
    np::initialize();

    py::register_exception_translator<calculate::CalculateException>(
      &calculate::translate_exception);

    py::def("calculate_rho_k",
            calculate::calculate_rho_k,
            "Calculate internal_calculate from k and x.",
            py::args("internal_calculate", "k", "x"));

    py::def("calculate_rho_j_k",
            calculate::calculate_rho_j_k,
            "Calculate internal_calculate and j_k from k, x, and v.",
            py::args("internal_calculate", "j_k", "k", "x", "v"));
}
