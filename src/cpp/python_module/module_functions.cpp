#include "module_functions.hpp"

#include <array_data_reference/array_data_reference.hpp>
#include <python_module/check_ndarray_properties.hpp>
#include <python_module/module_exception.hpp>
#include <internal_calculate/rho_k.hpp>

namespace np = boost::python::numpy;
namespace py = boost::python;


namespace calculate {

void
calculate_rho_k(np::ndarray& rho_k, const np::ndarray& k, const np::ndarray& x)
{
    ArrayDataReference rho_k_ref(rho_k, "internal_calculate");
    ArrayDataReference k_ref(k, "k");
    ArrayDataReference x_ref(x, "x");

    DataValueType data_value_type =
      check_mutually_compatible_data_value_types(rho_k_ref, k_ref, x_ref);

    check_complex_numbered(rho_k_ref);
    check_real_numbered(k_ref);
    check_real_numbered(x_ref);

    const auto [nk] = check_get_shape<1>(rho_k_ref);
    check_get_shape<2>(k_ref, 3, nk);
    check_get_shape<2>(x_ref, 3);

    auto rho_k_calculator = RhoK::implementation(ImplementationType::Naive, data_value_type);
    if (rho_k_calculator) {
        rho_k_calculator->calculate(rho_k_ref, k_ref, x_ref);
    } else {
        throw CalculateException("Could not find implementation");
    }
}


void
calculate_rho_j_k(np::ndarray& rho_k,
                  np::ndarray& j_k,
                  const np::ndarray& k,
                  const np::ndarray& x,
                  const np::ndarray& v)
{
    ArrayDataReference rho_k_ref(rho_k, "internal_calculate");
    ArrayDataReference j_k_ref(j_k, "j_k");
    ArrayDataReference k_ref(k, "k");
    ArrayDataReference x_ref(x, "x");
    ArrayDataReference v_ref(v, "v");

    check_complex_numbered(rho_k_ref);
    check_complex_numbered(j_k_ref);
    check_real_numbered(k_ref);
    check_real_numbered(x_ref);
    check_real_numbered(v_ref);

    check_mutually_compatible_data_value_types(rho_k_ref, j_k_ref, k_ref, x_ref, v_ref);

    const auto [nk] = check_get_shape<1>(rho_k_ref);
    check_get_shape<2>(j_k_ref, 3, nk);
    check_get_shape<2>(k_ref, 3, nk);
    const auto [nx] = check_get_shape<2>(x_ref, 3);
    check_get_shape<2>(v_ref, 3, nx);

    // TODO: call internal_calculate
}

}
