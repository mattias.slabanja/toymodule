#include "check_ndarray_properties.hpp"

#include <python_module/module_exception.hpp>
#include <array_data_reference/data_value_type.hpp>

//#include <boost/python/numpy/ndarray.hpp>
#include <fmt/format.h>

namespace np = boost::python::numpy;

namespace calculate
{

[[noreturn]] void
throw_data_value_type_mismatch(const std::vector<ArrayDataReference>& a_refs)
{
    auto to_string = [](const ArrayDataReference& a) {
      return fmt::format(
        FMT_STRING("{}:{}"), a.printable_name(), printable_name(a.data_value_type()));
    };
    std::vector<std::string> fragments;
    std::transform(begin(a_refs), end(a_refs), std::back_inserter(fragments), to_string);

    throw CalculateException(
      fmt::format(FMT_STRING("Data value type mismatch {}"), fmt::join(fragments, ", ")));
}



void
check_number_of_dimensions(const ArrayDataReference& a_ref, std::size_t expected_n_dimensions)
{
    if (a_ref.number_of_dimensions() != expected_n_dimensions) {
        throw CalculateException(
          fmt::format(FMT_STRING("{} should be {}-dimensional (is a {}-dimensional array)"),
                      a_ref.printable_name(),
                      expected_n_dimensions,
                      a_ref.number_of_dimensions()));
    }
}

[[noreturn]] void
throw_dimension_mismatch(const ArrayDataReference& a_ref,
                         std::size_t dimension_ix,
                         std::size_t expected_size)
{
    throw CalculateException(fmt::format(FMT_STRING("{} should be of size {} along "
                                                    "dimension {} (current size is {})"),
                                         a_ref.printable_name(),
                                         expected_size,
                                         dimension_ix,
                                         *a_ref.shape(dimension_ix)));
}

DataValueType
check_and_get_supported_datavalue(const ArrayDataReference& a_ref)
{
    const auto data_value_type = a_ref.data_value_type();
    if (data_value_type == DataValueType::Unsupported)
        throw CalculateException(fmt::format(
          FMT_STRING("{} has unsupported data value type (must be 32 or 64 bit floating point)"),
          a_ref.printable_name()));

    return data_value_type;
}

void
check_complex_numbered(const ArrayDataReference& a_ref)
{
    const bool is_complex = a_ref.data_number_type() == DataNumberType::Complex;
    if (!is_complex)
        throw CalculateException(
          fmt::format(FMT_STRING("{} does not hold complex-numbers"), a_ref.printable_name()));
}

void
check_real_numbered(const ArrayDataReference& a_ref)
{
    const bool is_real = a_ref.data_number_type() == DataNumberType::Real;
    if (!is_real)
        throw CalculateException(
          fmt::format(FMT_STRING("{} does not hold real-numbers"), a_ref.printable_name()));
}

}