#include "module_exception.hpp"

#include <boost/python/errors.hpp>

namespace calculate {

CalculateException::CalculateException(std::string message)
  : message_m(std::move(message))
{}

char const*
CalculateException::what() const noexcept
{
    return message_m.c_str();
}

void
translate_exception(const CalculateException& exception)
{
    PyErr_SetString(PyExc_RuntimeError, exception.what());
}

}
