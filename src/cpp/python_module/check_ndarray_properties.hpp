#pragma once

#include <array_data_reference/array_data_reference.hpp>

#include <boost/python/numpy/ndarray.hpp>
#include <type_traits>
#include <vector>

namespace calculate
{

void
check_complex_numbered(const ArrayDataReference& a_ref);

void
check_real_numbered(const ArrayDataReference& a_ref);

void
check_number_of_dimensions(const ArrayDataReference& a_ref, std::size_t expected_n_dimensions);

[[noreturn]] void
throw_dimension_mismatch(const ArrayDataReference& a_ref,
                         std::size_t dimension_ix,
                         std::size_t expected_size);

template<std::size_t NDimensions, typename... CheckSizes>
std::enable_if_t<std::conjunction_v<std::is_integral<CheckSizes>...>,
                 std::array<std::size_t, NDimensions - sizeof...(CheckSizes)>>
check_get_shape(const ArrayDataReference& a_ref, const CheckSizes&... check_sizes)
{
    check_number_of_dimensions(a_ref, NDimensions);
    std::size_t ix = 0;
    (void(check_sizes == *a_ref.shape(ix) ? void(++ix)
                                         : throw_dimension_mismatch(a_ref, ix, check_sizes)),
     ...);

    constexpr std::size_t remaining_dimensions = NDimensions - sizeof...(check_sizes);
    std::array<std::size_t, remaining_dimensions> result;
    for (std::size_t ix = 0; ix < remaining_dimensions; ++ix)
        result[ix] = *a_ref.shape(ix + sizeof...(check_sizes));

    return result;
}

DataValueType
check_and_get_supported_datavalue(const ArrayDataReference& a_ref);

[[noreturn]] void
throw_data_value_type_mismatch(const std::vector<ArrayDataReference>& a_refs);

template<typename... Args>
std::enable_if_t<std::conjunction_v<std::is_same<ArrayDataReference, Args>...>, DataValueType>
check_mutually_compatible_data_value_types(const ArrayDataReference& a_ref, const Args&... args)
{
    const DataValueType data_value_type = check_and_get_supported_datavalue(a_ref);
    if (((data_value_type == check_and_get_supported_datavalue(args)) && ...))
        return data_value_type;

    std::vector<ArrayDataReference> a_refs;
    a_refs.push_back(a_ref);
    (a_refs.push_back(args), ...);
    throw_data_value_type_mismatch(a_refs);
}

}