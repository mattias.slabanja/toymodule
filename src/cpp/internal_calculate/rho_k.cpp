#include "rho_k.hpp"

#include <internal_calculate/impl/naive_rho_k.hpp>
//#include <internal_calculate/impl/tbb_rho_k.hpp>
#include <internal_calculate/impl/openacc_rho_k.hpp>
#include <internal_calculate/impl/openmp_rho_k.hpp>
#include <internal_calculate/implementation_type.hpp>

#include <boost/type_index.hpp>
#include <spdlog/spdlog.h>

namespace calculate {

RhoK::~RhoK() = default;

namespace {

template<typename Implementation>
std::unique_ptr<RhoK>
if_implemented()
{
    auto p = std::make_unique<Implementation>();
    if (p && p->implemented())
        return p;

    spdlog::info("{} is not implemented",
                 boost::typeindex::type_id<Implementation>().pretty_name());
    return {};
}

template<template<typename> typename Implementation>
std::unique_ptr<RhoK>
data_specialization(DataValueType data_value_type)
{
    switch (data_value_type) {
        case DataValueType::Float: return if_implemented<Implementation<float>>();
        case DataValueType::Double: return if_implemented<Implementation<double>>();
        case DataValueType::LongDouble: return if_implemented<Implementation<long double>>();
        default:
            spdlog::error("Unsupported DataValueType {} given, no implementation found",
                          printable_name(data_value_type));
            break;
    }
    return {};
}

}

std::unique_ptr<RhoK>
RhoK::implementation(ImplementationType implementation_type, DataValueType data_value_type)
{
    switch (implementation_type) {
        case ImplementationType::Naive:
            return data_specialization<impl::NaiveRhoK>(data_value_type);
        case ImplementationType::OpenACC:
            return data_specialization<impl::OpenACCRhoK>(data_value_type);
        case ImplementationType::OpenMP:
            return data_specialization<impl::OpenMPRhoK>(data_value_type);
        default:
            spdlog::error("Unsupported Implementation {} given, no implementation found",
                          printable_name(implementation_type));
            break;
    }
    return {};
}

}

