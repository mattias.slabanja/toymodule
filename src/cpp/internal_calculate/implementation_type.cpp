#include "implementation_type.hpp"

namespace calculate {

std::string_view
printable_name(ImplementationType implementation_type)
{
    switch (implementation_type) {
        case (ImplementationType::Naive): return "Naive";
        case (ImplementationType::TBB): return "TBB";
        case (ImplementationType::OpenMP): return "OpenMP";
        case (ImplementationType::OpenACC): return "OpenACC";
        default: return "";
    }
}

}