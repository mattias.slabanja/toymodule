#pragma once

#include <array_data_reference/data_value_type.hpp>
#include <internal_calculate/implementation_type.hpp>

#include <memory>

namespace calculate {

class ArrayDataReference;

class RhoK {
  public:
    virtual ~RhoK();

    static std::unique_ptr<RhoK> implementation(ImplementationType implementation_type,
                                                DataValueType data_value_type);

    virtual bool implemented() const = 0;
    virtual void calculate(const ArrayDataReference& rho_k_ref,
                           const ArrayDataReference& k_ref,
                           const ArrayDataReference& x_ref) = 0;
};

}
