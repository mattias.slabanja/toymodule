#pragma once

#include <string_view>

namespace calculate {

enum class ImplementationType
{
    Naive,
    TBB,
    OpenMP,
    OpenACC
};

std::string_view
printable_name(ImplementationType implementation_type);

}