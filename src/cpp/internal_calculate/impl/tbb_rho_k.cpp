#include "tbb_rho_k.hpp"

#include <array_data_reference/array_data_reference.hpp>

#include <complex>
#include <numeric>
#include <cmath>
#include <tbb/tbb.h>
#include <tbb/parallel_reduce.h>



namespace calculate::impl {

template<typename Type>
void
TBBRhoK<Type>::calculate(const ArrayDataReference& rho_k_ref,
                         const ArrayDataReference& k_ref,
                         const ArrayDataReference& x_ref)
{
    using CType = std::complex<Type>;
    using A3Type = std::array<Type, 3>;

    constexpr Type oneType{1.0};

    auto exp_i = [](const A3Type& x, const A3Type& k) {
        const Type alpha = x[0] * k[0] + x[1] * k[1] + x[2] * k[2];
        return CType(std::cos(alpha), std::sin(alpha));
    };

    const auto rho_ks = *rho_k_ref.span<std::complex<Type>>();
    const auto ks = *k_ref.span<Type, 3>();
    const auto xs = *x_ref.span<Type, 3>();

    const Type factor = oneType / sqrt(xs.size());

    for (std::size_t k_ix = 0; k_ix < ks.size(); ++k_ix) {

        tbb::parallel_reduce()

        const auto& k = ks[k_ix];


        CType rho_k{};
        for (const auto& x : xs) {
            rho_k += exp_i(x, k);
        }
        rho_ks[k_ix] = factor * rho_k;
    }
}

template class TBBRhoK<float>;
template class TBBRhoK<double>;
template class TBBRhoK<long double>;

}
