#pragma once

#include <internal_calculate/rho_k.hpp>

namespace calculate::impl {

template <typename T>
class NaiveRhoK : public calculate::RhoK {
  public:
    void calculate(const ArrayDataReference& rho_k_ref,
                   const ArrayDataReference& k_ref,
                   const ArrayDataReference& x_ref) final;

    bool implemented() const final;
    using type = T;
};

}
