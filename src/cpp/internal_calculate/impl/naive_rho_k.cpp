#include "naive_rho_k.hpp"

#include <array_data_reference/array_data_reference.hpp>
#include <internal_calculate/impl/impl_utils.hpp>

#include <complex>
#include <cmath>


namespace calculate::impl {

template<typename Type>
void
NaiveRhoK<Type>::calculate(const ArrayDataReference& rho_k_ref,
                           const ArrayDataReference& k_ref,
                           const ArrayDataReference& x_ref)
{
    using CType = std::complex<Type>;

    const std::span<CType> rho_ks = *rho_k_ref.as_span<CType>();
    const std::span<Type[3]> ks = *k_ref.as_span<Type[3]>();
    const std::span<Type[3]> xs = *x_ref.as_span<Type[3]>();

    const Type factor = Type{1.0} / sqrt(xs.size());

    for (std::size_t k_ix = 0; k_ix < ks.size(); ++k_ix)
    {
        const auto& k = ks[k_ix];
        CType rho_k {};
        for (const auto& x : xs) {
            const Type alpha = dot(x, k);
            rho_k += CType{std::cos(alpha), std::sin(alpha)};
        }
        rho_ks[k_ix] = factor * rho_k;
    }
}

template<typename Type>
bool
NaiveRhoK<Type>::implemented() const
{
    return true;
}

template class NaiveRhoK<float>;
template class NaiveRhoK<double>;
template class NaiveRhoK<long double>;

}
