#include "openmp_rho_k.hpp"

#include <array_data_reference/array_data_reference.hpp>

#include <complex>
#include <numeric>
#include <cmath>


namespace calculate::impl {

template<typename Type>
void
OpenMPRhoK<Type>::calculate(const ArrayDataReference& rho_k_ref,
                           const ArrayDataReference& k_ref,
                           const ArrayDataReference& x_ref)
{
    using CType = std::complex<Type>;

    const std::span<CType> rho_ks = *rho_k_ref.as_span<CType>();
    const std::span<Type[3]> ks = *k_ref.as_span<Type[3]>();
    const std::span<Type[3]> xs = *x_ref.as_span<Type[3]>();

    const Type factor = Type{1.0} / sqrt(xs.size());

#pragma omp parallel for shared(rho_ks, xs, ks, factor) default(none)
    for (std::size_t k_ix = 0; k_ix < ks.size(); ++k_ix)
    {
        const auto& k = ks[k_ix];
        CType rho_k {};
        for (const auto& x : xs) {
            const Type alpha = x[0]*k[0] + x[1]*k[1] + x[2]*k[2];
            rho_k += CType{std::cos(alpha), std::sin(alpha)};
        }
        rho_ks[k_ix] = factor * rho_k;
    }
}

template<typename Type>
bool
OpenMPRhoK<Type>::implemented() const
{
    return true;
}

template class OpenMPRhoK<float>;
template class OpenMPRhoK<double>;
template class OpenMPRhoK<long double>;

}
