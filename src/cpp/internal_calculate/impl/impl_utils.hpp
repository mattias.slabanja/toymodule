#pragma once

#include <type_traits>

namespace calculate::impl {

template<typename TA,
         std::enable_if_t<std::is_array_v<TA>, int> = 0,
         typename T = std::remove_extent_t<TA>,
         std::size_t N = std::extent_v<TA>>
inline T
dot(const TA& a, const TA& b)
{
    T result{};
    for (std::size_t ix = 0; ix < N; ++ix)
        result += a[ix] * b[ix];

    return result;
}

}