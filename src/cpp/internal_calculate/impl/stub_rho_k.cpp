#ifndef STUBNAME
#error "STUBNAME not defined"
#endif

#include <internal_calculate/rho_k.hpp>

namespace calculate::impl {

template <typename T>
class STUBNAME : public calculate::RhoK {
  public:
    inline void calculate(const ArrayDataReference&,
                          const ArrayDataReference&,
                          const ArrayDataReference&) final
    {}
    inline bool implemented() const final { return false; }
    using type = T;
};

template class STUBNAME<float>;
template class STUBNAME<double>;
template class STUBNAME<long double>;

}
