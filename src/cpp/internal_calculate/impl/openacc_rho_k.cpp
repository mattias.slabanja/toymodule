#include "openacc_rho_k.hpp"

#include <array_data_reference/array_data_reference.hpp>

#include <complex>
#include <numeric>
#include <cmath>


namespace calculate::impl {

template<typename Type>
void
OpenACCRhoK<Type>::calculate(const ArrayDataReference& rho_k_ref,
                             const ArrayDataReference& k_ref,
                             const ArrayDataReference& x_ref)
{
    using CType = std::complex<Type>;

    const std::span<CType> rho_ks = *rho_k_ref.as_span<CType>();
    const std::span<Type[3]> ks = *k_ref.as_span<Type[3]>();
    const std::span<Type[3]> xs = *x_ref.as_span<Type[3]>();

    const Type factor = Type{1.0} / sqrt(xs.size());

    const auto xs_p = xs.data();
    const auto xs_size = xs.size();
    const auto ks_p = ks.data();
    const auto ks_size = ks.size();
    const auto rho_ks_p = rho_ks.data();

#pragma acc kernels copyin(xs_p[0:xs_size], ks_p[0:ks_size]) copyout(rho_ks_p[0:ks_size])
    {
        for (std::size_t k_ix = 0; k_ix < ks_size; ++k_ix) {
            const auto& k = ks_p[k_ix];
            CType rho_k{};
            for (std::size_t x_ix = 0; x_ix < xs_size; ++x_ix) {
                const auto& x = xs_p[x_ix];
                const Type alpha = x[0] * k[0] + x[1] * k[1] + x[2] * k[2];
                rho_k += CType{std::cos(alpha), std::sin(alpha)};
            }
            rho_ks_p[k_ix] = factor * rho_k;
        }
    }
}

template<typename Type>
bool
OpenACCRhoK<Type>::implemented() const
{
    return true;
}

template class OpenACCRhoK<float>;
template class OpenACCRhoK<double>;
template class OpenACCRhoK<long double>;


}
