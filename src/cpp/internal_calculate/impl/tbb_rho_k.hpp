#pragma once

#include <internal_calculate/rho_k.hpp>

namespace calculate::impl {

template <typename T>
class TBBRhoK : public calculate::RhoK {
  public:
    void calculate(const ArrayDataReference& x,
                   const ArrayDataReference& k,
                   const ArrayDataReference& x_ref) final override;
};

}
